	.global _start
	
_start:

@Prompt user to enter a character

	MOV R7, #4
	MOV R0, #1
	MOV R2, #26
	LDR R1, =string1
	SWI 0
	
@Get input from keyboard

_read:
	MOV R7, #3
	MOV R0, #0
	MOV R2, #5
	LDR R1, =input
	SWI 0
	
@Get the character

	LDR R2, =input
	LDRB R0, [R2, #0]
	
@If charater is a space

	CMP R0, #32
	BEQ _space
	BNE _next1
	
@Then
@Display message "You entered a space"

_space:
	
	MOV R7, #4
	MOV R0, #1
	MOV R2, #20
	LDR R1, =string2
	SWI 0
	
@Set return to 110

	MOV R0, #110
	BAL _exit
	
@Else
@If character is an Ascii value for a number 0 through 9

_next1:

	MOV R1, #48

_loop1:
	
	CMP R0, R1
	BEQ _number
	ADD R1, R1, #1
	CMP R1, #58
	BEQ _next2
	BAL _loop1
	
@Then
@Display message "You entered a number"

_number:

	MOV R7, #4
	MOV R0, #1
	MOV R2, #21
	LDR R1, =string3
	SWI 0
	
@Set return code to 120

	MOV R0, #120
	BAL _exit
	
@Else
@If character is lower case letter between a and z

_next2:

	MOV R1, #97
	
_loop2:
	
	CMP R0, R1
	BEQ _lowercase
	ADD R1, R1, #1
	CMP R1, #123
	BEQ _next3
	BAL _loop2
	
@Then
@Display message "You entered a lower case letter"

_lowercase:

	MOV R7, #4
	MOV R0, #1
	MOV R2, #32
	LDR R1, =string4
	SWI 0
	
@Set return code to 130

	MOV R0, #130
	BAL _exit
	
@Else
@If character is an upper case letter between A and Z
	
_next3:

	MOV R1, #65
	
_loop3:
	
	CMP R0, R1
	BEQ _uppercase
	ADD R1, R1, #1
	CMP R1, #91
	BEQ _next4
	BAL _loop3
	
@Display message "You entered an upper case letter"

_uppercase:

	MOV R7, #4
	MOV R0, #1
	MOV R2, #33
	LDR R1, =string5
	SWI 0
	
@Set return code to 140

	MOV R0, #140
	BAL _exit

@Else
@If character is a control code

_next4:

	MOV R1, #0
	
_loop4:
	
	CMP R0, R1
	BEQ _concode
	ADD R1, R1, #1
	CMP R1, #32
	BEQ _next5
	BAL _loop4
	

@Then
@Display message "You entered a control code"

_concode:

	MOV R7, #4
	MOV R0, #1
	MOV R2, #27
	LDR R1, =string6
	SWI 0
	
@Set return code to 150

	MOV R0, #150
	BAL _exit
	
@Else
@Assume you have a punctuation character
@Display the message "You have entered a punctuation character"


_next5:

	MOV R7, #4
	MOV R0, #1
	MOV R2, #41
	LDR R1, =string7
	SWI 0
	
@Set return code to 160

	MOV R0, #160

_exit:

	MOV R7, #1
	SWI 0
	
.data
string1:
.ascii "Please enter a character:\n"
string2:
.ascii "You entered a space\n"
string3:
.ascii "You entered a number\n"
string4:
.ascii "You entered a lower case letter\n"
string5:
.ascii "You entered an upper case letter\n"
string6:
.ascii "You entered a control code\n"
string7:
.ascii "You have entered a punctuation character\n"
input:
.ascii " \n"
